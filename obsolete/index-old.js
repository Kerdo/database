const express = require('express');
const port = 5505;

const app = express();
const router = express.Router();

const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const functions = require('./functions');

const admin = require('firebase-admin');
admin.initializeApp({
  apiKey: 'AIzaSyC5clg9ApLT_pTKepYn66aPiZg7KqFM87w',
  authDomain: 'kerdodotme.firebaseapp.com',
  databaseURL: 'https://kerdodotme.firebaseio.com',
  projectId: 'kerdodotme',
  storageBucket: 'kerdodotme.appspot.com',
  messagingSenderId: '470135457314'
});

const firestore = admin.firestore();
firestore.settings({ timestampsInSnapshots: true });

app.set('view engine', 'ejs');
app.use(cookieParser());
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

const userCollection = 'users';

router.route('/').get((req, res) => {
  res.render('index');
});

router.route('/account').get((req, res) => {
  const loggedIn = req.cookies.logged_in;
  const uid = req.cookies.uid;

  if (loggedIn && uid && uid !== 'undefined') {
    firestore
      .doc(`${userCollection}/${uid}`)
      .get()
      .then(doc => {
        if (doc && doc.exists) {
          const data = doc.data();

          res.render('account', {
            username: data.username,
            email: data.email,
            password: data.password
          });
        }
      });
  } else {
    res.redirect('/login');
  }
});

router.route('/login').get((req, res) => {
  const loggedIn = req.cookies.logged_in;
  const uid = req.cookies.uid;
  if (loggedIn && uid && uid !== 'undefined') {
    res.redirect('/account');
  } else {
    res.render('login');
  }
});

router.route('/login').post((req, res) => {
  if (req.body.username && req.body.password) {
    const username = req.body.username;
    const password = functions.hash('sha512', req.body.password);

    firestore
      .collection(userCollection)
      .where('username', '==', username)
      .limit(1)
      .get()
      .then(snap => {
        if (!snap.empty) {
          //User exists. Handle
          snap.forEach(doc => {
            if (doc && doc.exists) {
              const data = doc.data();

              const uid = doc.id;

              if (password === data.password) {
                //Correct password. Handle
                firestore
                  .doc(`${userCollection}/${uid}`)
                  .update({ lastLogin: new Date() })
                  .then(() => {
                    res
                      .cookie('logged_in', true)
                      .cookie('uid', uid)
                      .send('Logged in!');
                  })
                  .catch(err => {
                    res.send(err);
                  });
              } else {
                //Wrong password. Handle
                res.send('Wrong password!');
              }
            } else {
              //User doesn't exists. Handle
              res.send("User doesn't exist");
            }
          });
        } else {
          //User doesn't exists. Handle
          res.send("User doesn't exist");
        }
      })
      .catch(err => {
        res.send(err);
      });
  } else {
    res.redirect('/login');
  }
});

router.route('/register').get((req, res) => {
  const loggedIn = req.cookies.logged_in;
  const uid = req.cookies.uid;
  if (loggedIn && uid && uid !== 'undefined') {
    res.redirect('/account');
  } else {
    res.render('register');
  }
});

router.route('/register').post((req, res) => {
  const body = req.body;
  if (body.username && body.email && body.password1 && body.password2) {
    const username = body.username;
    const email = body.email;
    const password1 = body.password1;
    const password2 = body.password2;

    firestore
      .collection(userCollection)
      .where('username', '==', username)
      .get()
      .then(snap => {
        if (snap.empty) {
          if (password1 === password2) {
            const password = functions.hash('sha512', password1);
            const uid = functions.randomString(24);
            firestore
              .collection(userCollection)
              .doc(uid)
              .set({
                username: username,
                email: email,
                password: password,
                joined: new Date(),
                lastLogin: new Date(),
                role: 'user'
              })
              .then(() => {
                res.send('Successfully registered!');
              })
              .catch(err => {
                res.send(err);
              });
          } else {
            res.send("Passwords don't match");
          }
        } else {
          res.send('User already exists');
        }
      });
  } else {
    res.redirect('/register');
  }
});

router.route('/logout').get((req, res) => {
  res.clearCookie('logged_in');
  res.clearCookie('uid');
  res.redirect('/login');
});

router.route('/admin').get((req, res) => {
  if (functions.userLoggedIn(req)) {
    functions
      .userAllowed(firestore, userCollection, req, 'admin')
      .then(allowed => {
        if (allowed) {
          const users = [];

          firestore
            .collection(userCollection)
            .get()
            .then(snap => {
              snap.forEach(doc => {
                if (doc && doc.exists) users.push(doc.data());
              });
              res.render('admin', { users: users });
            });
        } else res.send('No permission.');
      });
  } else res.redirect('/login');
});

router.route('*').get((req, res) => {
  res.status(404).send('4-0-4');
});

app.use('/', router);

app.listen(port, () => {
  console.log('Server listening on port 5505.');
});
