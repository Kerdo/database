'use strict';

const express = require('express');
const port = 5505;

const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const crypto = require('crypto');

const request = require('request');

const database = require('./database');

const functions = require('./functions');

const app = express();
const router = express.Router();

app.use(cookieParser());
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.set('view engine', 'ejs');

router.route('/').get((req, res) => {
  const loggedIn = req.cookies.loggedIn;
  if (loggedIn) res.send('data');
  else res.redirect('/login');
});

router.route('/login').get((req, res) => {
  res.render('login');
});

router.route('/login').post(async (req, res) => {
  let password = hash(req.body.password);
  const { username } = req.body;

  const data = {
    db: 'users',
    action: 'get',
    path: 'data.users',
    where: ['username', '==', username],
    limit: 1
  };

  let response = await fetchDatabase(data).then(body => body);

  response = JSON.parse(response);

  if (response.status == 200 && response.type === 'success') {
    if (response.data[0].password === password) {
      res.send('Loggedin');
    } else {
      res.send('wrong password');
    }
  } else if (response.status == 400 && response.type === 'fail') {
    res.send("User doesn't exist");
  }
});

router.route('/register').get((req, res) => {
  res.render('register');
});

router.route('/register').post(async (req, res) => {
  const { username } = req.body;
  let { password, password1 } = req.body;
  const uid = functions.randomString(12);

  if (password === password1) {
    password = hash(password);
    const data = {
      db: 'users',
      action: 'get',
      path: 'data.users',
      where: ['username', '==', username]
    };

    const body = await fetchDatabase(data).then(body => JSON.parse(body));

    if (body.data.length == 0) {
      const body1 = await fetchDatabase({
        db: 'users',
        action: 'set',
        path: `data.users.${uid}`,
        data: {
          username,
          password
        }
      }).then(body => body);

      res.send(`<pre>${body1}</pre>`);
    } else {
      res.send('User exists');
    }
  } else {
    res.send("Passwords don't match.");
  }
});

app.use('/', router);

app.use('/db', database.router);

app.listen(port);

async function fetchDatabase(data) {
  return new Promise((res, rej) => {
    request(
      {
        headers: {
          'Content-Type': 'application/json'
        },
        uri: 'http://localhost:5505/db',
        body: JSON.stringify(data),
        method: 'POST'
      },
      (err, resp, body) => {
        if (!err && body) res(body);
        else rej(err);
      }
    );
  });
}

function hash(data) {
  return crypto
    .createHash('sha512')
    .update(data)
    .digest('hex');
}
