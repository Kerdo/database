const crypto = require('crypto');

const request = require('request');

const hash = (hash, data) => {
  return crypto
    .createHash(hash)
    .update(data)
    .digest('hex');
};

const randomString = len => {
  let text = '';
  const possible =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (let i = 0; i < len; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  return text;
};

const userLoggedIn = req => {
  const loggedIn = req.cookies.logged_in;
  const uid = req.cookies.uid;

  return loggedIn && uid && uid !== 'undefined';
};

const hasRole = async (uid, role) => {
  const body = await fetchDatabase({
    db: 'users',
    action: 'get',
    path: `data.users.${uid}`,
    limit: 1
  }).then(body => body);

  const userRole = body.data.role;

  if (role.constructor === Array) return role.indexOf(userRole) !== -1;
  else if (role.constructor === String) return userRole === role;
  return false;
};

const fetchDatabase = async data => {
  return new Promise((res, rej) => {
    request(
      {
        headers: {
          'Content-Type': 'application/json'
        },
        uri: 'http://localhost:5505/db/fetch',
        body: JSON.stringify(data),
        method: 'POST'
      },
      (err, resp, body) => {
        if (!err && body) res(JSON.parse(body));
        else rej(err, resp);
      }
    );
  });
};

const currentTime = () => {
  const now = new Date();

  let hours = now.getHours();
  if (parseInt(hours) < 10) hours = '0' + hours;

  let minutes = now.getMinutes();
  if (parseInt(minutes) < 10) minutes = '0' + minutes;

  let seconds = now.getSeconds();
  if (parseInt(seconds) < 10) seconds = '0' + seconds;

  let day = now.getDate();
  if (parseInt(day) < 10) day = '0' + secodaynds;

  let month = now.getMonth();
  if (parseInt(month) < 10) month = '0' + month;

  let year = now
    .getFullYear()
    .toString()
    .substr(2, 2);
  if (parseInt(year) < 10) year = '0' + year;

  return (
    '[' +
    hours +
    ':' +
    minutes +
    ':' +
    seconds +
    ' ' +
    day +
    '/' +
    month +
    '/' +
    year +
    ']'
  );
};

module.exports = {
  hash,
  randomString,
  userLoggedIn,
  hasRole,
  fetchDatabase,
  currentTime
};
