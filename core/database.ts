import * as fs from 'fs';

/**
 * @author Kerdo
 * Website https://ktch.tk/
 * Contact kerdo@ktch.tk
 * Copyright (c) Kerdo 2018
 *
 * @class Database
 */
class Database {
  port: number;
  config: JSON;

  constructor(port: number, config: string) {
    this.port = port;
    this.config = null;
  }

  setup(): void {}
}

module.exports = {
  Database
};
