/**
 * @author Kerdo
 * Website https://ktch.tk/
 * Contact kerdo@ktch.tk
 * Copyright (c) Kerdo 2018
 */

'use strict';

const port = 5505;

const express = require('express');

const router = express.Router();

const functions = require('./functions');

router.route('/').get(async (req, res) => {
  const { uid, logged_in } = req.cookies;

  let body = null;
  if (uid) {
    body = await functions
      .fetchDatabase({
        db: 'users',
        action: 'get',
        path: `data.users.${uid}`
      })
      .then(body => body);
  } else body = { status: 400 };

  res.render('index', {
    loggedIn: logged_in == 'true' ? true : false,
    username: body.status == 200 ? body.data.username : 'null'
  });
});

router.route('/account').get(async (req, res) => {
  const loggedIn = req.cookies.logged_in;
  const uid = req.cookies.uid;

  if (loggedIn && uid && uid !== 'undefined') {
    const body = await functions
      .fetchDatabase({
        db: 'users',
        action: 'get',
        path: `data.users.${uid}`,
        limit: 1
      })
      .then(body => body)
      .catch((err, resp) => {
        console.error(err, resp);
      });

    const { username, email, password } = body.data;

    res.render('account', { username, email, password, uid });
  } else {
    res.redirect('/login?next=/account');
  }
});

router.route('/login').get((req, res) => {
  const loggedIn = req.cookies.logged_in;
  const uid = req.cookies.uid;
  if (loggedIn && uid && uid !== 'undefined') {
    res.redirect('/account');
  } else {
    res.render('login', {
      next: req.query.next == null ? '/' : unescape(req.query.next)
    });
  }
});

router.route('/login').post(async (req, res) => {
  const next = unescape(req.body.next || '/');

  if (req.body.username && req.body.password) {
    const username = req.body.username;
    const password = functions.hash('sha512', req.body.password);

    const body = await functions
      .fetchDatabase({
        db: 'users',
        action: 'get',
        path: 'data.users',
        where: ['username', '==', username]
      })
      .then(body => body)
      .catch((err, resp) => {
        console.error(err, resp);
      });

    if (body.data && body.data.length > 0) {
      if (body.data[0].password === password) {
        res
          .cookie('logged_in', true)
          .cookie('uid', body.data[0].uid)
          .redirect(next);
      } else {
        res.send('Wrong password.');
      }
    } else {
      res.send("User doesn't exist.");
    }
  } else {
    res.redirect('/login?next=' + next);
  }
});

router.route('/register').get((req, res) => {
  const loggedIn = req.cookies.logged_in;
  const uid = req.cookies.uid;
  if (loggedIn && uid && uid !== 'undefined') {
    res.redirect('/account');
  } else {
    res.render('register');
  }
});

router.route('/register').post(async (req, res) => {
  const { username, email, password, password1, role } = req.body;
  const uid = functions.randomString(24);
  if (username && email && password && password1) {
    if (password === password1) {
      const body = await functions
        .fetchDatabase({
          db: 'users',
          action: 'get',
          path: 'data.users',
          where: ['username', '==', username]
        })
        .then(body => body)
        .catch((err, resp) => {
          console.error(err, resp);
        });

      if (body.data && body.data.length == 0) {
        const body1 = await functions
          .fetchDatabase({
            db: 'users',
            action: 'set',
            path: `data.users.${uid}`,
            data: {
              username,
              password: functions.hash('sha512', password),
              email,
              uid,
              role: role == null ? 'user' : role
            }
          })
          .then(body => body)
          .catch((err, resp) => {
            console.error(err, resp);
          });

        if (body1.data && body1.data === 'Set.') {
          res.redirect('/login');
        } else {
          res.send('An unknown error has occured.');
        }
      } else {
        res.send('User already exists.');
      }
    } else {
      res.send("Passwords dont't match.");
    }
  } else {
    res.redirect('/register');
  }
});

router.route('/logout').get((req, res) => {
  res.clearCookie('logged_in');
  res.clearCookie('uid');
  res.redirect('/login');
});

router.route('/users').get(async (req, res) => {
  if (functions.userLoggedIn(req)) {
    if (
      await functions.hasRole(req.cookies.uid, ['system', 'admin', 'manager'])
    ) {
      const body = await functions
        .fetchDatabase({
          db: 'users',
          action: 'get',
          path: 'data.users'
        })
        .then(body => body)
        .catch((err, resp) => {
          console.error(err, resp);
        });

      const roles = await functions
        .fetchDatabase({
          db: 'users',
          action: 'get',
          path: 'info.roles'
        })
        .then(body => body)
        .catch((err, resp) => {
          console.error(err, resp);
        });

      if (body.status == 200 && body.data) {
        const users = [];
        for (let key in body.data) users.push(body.data[key]);

        res.render('users', {
          users,
          roles: roles.data,
          userUid: req.cookies.uid
        });
      } else {
        res.send('test');
      }
    } else {
      res.send('Invalid permissions.');
    }
  } else res.redirect('/login?next=/users');
});

router.route('/user/:uid/:op*?').get(async (req, res) => {
  const { uid, op } = req.params;

  if (uid) {
    if (req.cookies.uid === uid) {
      res.redirect('/account');
    } else {
      if (op === 'delete') {
        const body = await functions
          .fetchDatabase({
            db: 'users',
            action: 'get',
            path: `data.users.${uid}.role`
          })
          .then(body => body)
          .catch((err, resp) => {
            console.error(err, resp);
          });

        if (body.status == 200) {
          if (body.data == 'system') {
            res.send('Cannot delete that user');
          } else {
            functions
              .fetchDatabase({
                db: 'users',
                action: 'delete',
                path: `data.users.${uid}`
              })
              .then(body => {
                res.redirect('/users');
              })
              .catch((err, resp) => {
                console.error(err, resp);
              });
          }
        } else {
          res.send('An unknown error has occurred.');
        }
      } else {
        const body = await functions
          .fetchDatabase({
            db: 'users',
            action: 'get',
            path: `data.users.${uid}`,
            limit: 1
          })
          .then(body => body)
          .catch((err, resp) => {
            console.error(err, resp);
          });

        if (body.status == 200 && body.data) {
          const { username, email, uid, password } = body.data;

          res.render('user', { username, email, uid, password });
        } else {
          res.redirect('/users');
        }
      }
    }
  } else {
    res.redirect('/users');
  }
});

router.route('/databases').get(async (req, res) => {
  if (functions.userLoggedIn(req)) {
    const { uid } = req.cookies;

    const body = await functions
      .fetchDatabase({
        db: '../databases',
        action: 'get',
        path: 'databases',
        where: ['authUid', '==', uid]
      })
      .then(body => body)
      .catch((err, resp) => {
        console.error(err, resp);
      });

    let databases = [];
    if (body.status == 200 && body.data.length > 0) {
      for (let i = 0; i < body.data.length; i++) {
        const data = await functions
          .fetchDatabase({
            db: body.data[i].path,
            action: 'get',
            path: '.'
          })
          .then(body => body)
          .catch((err, resp) => {
            console.error(err, resp);
          });

        if (data.status == 200 && data.data) databases.push(data.data);
      }
    }

    res.render('databases', { databases, userId: uid });
  } else {
    res.redirect('/login');
  }
});

router.route('/database/:uid/:op*?').get(async (req, res) => {
  const { uid, op } = req.params;

  if (uid && op == 'delete') {
    const body = await functions
      .fetchDatabase({
        db: '../databases',
        action: 'get',
        path: `databases.${uid}`
      })
      .then(body => body)
      .catch((err, resp) => {
        console.error(err, resp);
      });

    if (body && body.status == 200 && body.data) {
      const { path } = body.data;

      const data = await functions
        .fetchDatabase({
          db: path,
          action: 'drop'
        })
        .then(body => body);

      if (data && data.status == 200) {
        res.send(data.data);
      } else {
        res.send("Database couldn't be dropped.");
      }
    } else {
      res.send("Database not found or couldn't be deleted");
    }
  } else {
    if (uid) {
      const body = await functions
        .fetchDatabase({
          db: '../databases',
          action: 'get',
          path: `databases.${uid}`
        })
        .then(body => body)
        .catch((err, resp) => {
          console.error(err, resp);
        });

      if (body.status == 200 && body.data) {
        const data = await functions
          .fetchDatabase({
            db: body.data.path,
            action: 'get',
            path: '.'
          })
          .then(body => body)
          .catch((err, resp) => {
            console.error(err, resp);
          });

        if (data.status == 200 && data.data) {
          res.render('database', { database: data.data });
        } else {
          res.redirect('/databases');
        }
      } else {
        res.redirect('/databases');
      }
    } else {
      res.redirect('/databases');
    }
  }
});

router.route('/database-create').post(async (req, res) => {
  const { name, description, roles, userId } = req.body;

  if (name && description && roles && userId) {
    const body = await functions
      .fetchDatabase({
        db: '',
        action: 'create',
        data: {
          name,
          description,
          roles,
          userId
        }
      })
      .then(body => body);

    if (body && body.status == 200) {
      res.redirect('/databases');
    } else {
      res.send('error has occurred');
    }
  } else {
    res.redirect('/databases');
  }
});

router.route('*').get((req, res) => {
  res.status(404).send('4-0-4');
});

const log = (req, res, next) => {
  console.log(
    functions.currentTime() +
      ' [WEB, ' +
      req.method +
      '] ' +
      req.protocol +
      '://' +
      req.hostname +
      req.url
  );
  next();
};

module.exports = {
  router,
  port,
  log
};
