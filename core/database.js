const express = require('express');
const fs = require('fs');

const functions = require('./functions');

const router = express.Router();

const config = JSON.parse(
  fs.readFileSync(__dirname + '/../database/config.json')
);

const rulesFileExt = config.rules;

let root = {};

router.route('/fetch').post((req, res) => {
  const { db, action, path, data, where, limit } = req.body;

  if (db && db != '') {
    const dbFile = __dirname + `${config.data_folder}${db}.json`;
    const rulesFile =
      __dirname + `${config.data_folder}` + rulesFileExt.replace(/<db>/, db);

    const str = fs.readFileSync(dbFile);

    root = JSON.parse(str);

    if (action === 'get') {
      if (path === '.') tree = root;
      else tree = get(path, where, limit);
    } else if (action === 'set') {
      set(path, data);
      fs.writeFileSync(dbFile, JSON.stringify(root));
      tree = 'Set.';
    } else if (action === 'delete') {
      set(path, null);
      fs.writeFileSync(dbFile, JSON.stringify(root));
      tree = 'Deleted.';
    } else if (action === 'drop') {
      const dbsFile = __dirname + `${config.data_folder}../databases.json`;

      let temp = JSON.parse(fs.readFileSync(dbsFile));

      tree = "Database doesn't exist";
      for (obj in temp.databases) {
        if (temp.databases[obj].path === db) {
          if (temp.databases[obj].allow_delete) {
            fs.unlinkSync(dbFile);
            fs.unlinkSync(rulesFile);
            delete temp.databases[obj];

            temp = JSON.stringify(temp);
            fs.writeFileSync(dbsFile, temp);

            tree = 'Dropped.';
            break;
          } else {
            tree = 'Database cannot be dropped.';
            break;
          }
        }
      }
    } else {
      if (path === '.') tree = root;
      else tree = get(path, where, limit);
    }

    if (tree == undefined) {
      tree = {
        status: 400,
        type: 'fail',
        data: "Field doesn't exist"
      };
    } else {
      tree = {
        status: 200,
        type: 'success',
        data: tree
      };
    }

    console.log(functions.currentTime() + ' [DB]', JSON.stringify(req.body));

    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(tree));
  } else {
    if (action === 'create') {
      let { name, description, roles, userId } = data;

      if (name && description && roles && userId) {
        roles = roles.split(',');
        const uid = functions.randomString(8);
        const fileName = functions.randomString(6);

        const database = {
          info: {
            name,
            description,
            roles,
            ownerUid: userId,
            version: 1,
            uid
          },
          data: {}
        };

        fs.writeFileSync(
          __dirname + `${config.data_folder}${fileName}.json`,
          JSON.stringify(database)
        );
        fs.writeFileSync(
          __dirname + `${config.data_folder}${fileName}.rules.jsonc`,
          JSON.stringify({})
        );

        const dbsFile = __dirname + `${config.data_folder}../databases.json`;
        let temp = JSON.parse(fs.readFileSync(dbsFile));

        temp.databases[uid] = {
          path: fileName,
          authUid: userId,
          allow_delete: true
        };

        temp = JSON.stringify(temp);
        fs.writeFileSync(dbsFile, temp);

        res.send(
          JSON.stringify({
            status: 200,
            type: 'success',
            data: 'Created.'
          })
        );
      } else {
        res.send('error1');
      }
    } else {
      res.send('error');
    }
  }
});

function get(path, where, limit) {
  //FIXME: Implement security rules.
  if (limit) amt = 0;
  if (where) {
    let obj = get(path);
    let tree = [];
    for (el in obj) {
      if (limit && amt >= limit) break;
      el = obj[el];
      //TODO: Fix if-else-hell
      if (where[1] === '==') {
        if (el[where[0]] == where[2]) {
          tree.push(el);
          if (limit) amt++;
        }
      } else if (where[1] === '!=') {
        if (el[where[0]] != where[2]) {
          tree.push(el);
          if (limit) amt++;
        }
      } else if (where[1] === '>') {
        if (el[where[0]] > where[2]) {
          tree.push(el);
          if (limit) amt++;
        }
      } else if (where[1] === '>=') {
        if (el[where[0]] >= where[2]) {
          tree.push(el);
          if (limit) amt++;
        }
      } else if (where[1] === '<') {
        if (el[where[0]] < where[2]) {
          tree.push(el);
          if (limit) amt++;
        }
      } else if (where[1] === '<=') {
        if (el[where[0]] <= where[2]) {
          tree.push(el);
          if (limit) amt++;
        }
      }
    }
    return tree;
  } else {
    return path.split('.').reduce(function(prev, curr) {
      return prev ? prev[curr] : null;
    }, root || self);
  }
}

function set(path, value) {
  //FIXME: Implement security rules.
  const pList = path.split('.');
  const key = pList.pop();
  const pointer = pList.reduce((accumulator, currentValue) => {
    if (accumulator[currentValue] === undefined) accumulator[currentValue] = {};
    return accumulator[currentValue];
  }, root);
  if (value) pointer[key] = value;
  else delete pointer[key];
  return root;
}

function rules(path, action) {
  /*  path = 'info.rules.' + path + '.*.' + action;
  console.log(path);
  return path.split('.').reduce(function(prev, curr) {
    return prev ? prev[curr] : null;
  }, root || self); */
  return true;
}

module.exports = {
  router,
  config
};
