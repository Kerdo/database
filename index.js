/**
 * @author Kerdo
 * Website https://ktch.tk/
 * Contact kerdo@ktch.tk
 * Copyright (c) Kerdo 2018
 */

'use strict';

const express = require('express');

const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const index = require('./core/app');
const database = require('./core/database');
const functions = require('./core/functions');

const app = express();

app.set('view engine', 'ejs');
app.use(cookieParser());
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.use(index.log);

app.use('/', index.router);
app.use('/db', database.router);

app.listen(index.port, () => {
  console.log('[WEB/DB] Listening on port %s.', index.port);
});
